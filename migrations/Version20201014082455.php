<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20201014082455 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Set workspace slugged name unique';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE t_workspace CHANGE slugged_name slugged_name VARCHAR(150) NOT NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_6FE59EFF4C4D89 ON t_workspace (slugged_name)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP INDEX UNIQ_6FE59EFF4C4D89 ON t_workspace');
        $this->addSql('ALTER TABLE t_workspace CHANGE slugged_name slugged_name VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
    }
}
