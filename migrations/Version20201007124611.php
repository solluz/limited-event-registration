<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20201007124611 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Initial schema';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE t_event (
          id INT AUTO_INCREMENT NOT NULL, 
          workspace_id INT DEFAULT NULL, 
          name VARCHAR(150) NOT NULL, 
          description LONGTEXT NOT NULL, 
          registration_capacity INT NOT NULL, 
          registration_end_at DATETIME NOT NULL, 
          created_at DATETIME NOT NULL, 
          updated_at DATETIME NOT NULL, 
          INDEX IDX_851F6CDE82D40A1F (workspace_id), 
          UNIQUE INDEX workspace_event_name (workspace_id, name), 
          PRIMARY KEY(id)
        ) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');

        $this->addSql('CREATE TABLE t_registration (
          id INT AUTO_INCREMENT NOT NULL, 
          event_id INT DEFAULT NULL, 
          first_name VARCHAR(150) NOT NULL, 
          last_name VARCHAR(150) NOT NULL, 
          email VARCHAR(255) DEFAULT NULL, 
          phone_number VARCHAR(50) DEFAULT NULL, 
          registered_at DATETIME NOT NULL, 
          registered_by VARCHAR(150) DEFAULT NULL, 
          created_at DATETIME NOT NULL, 
          updated_at DATETIME NOT NULL, 
          INDEX IDX_15FFC61571F7E88B (event_id), 
          UNIQUE INDEX event_registration_name (event_id, first_name, last_name), 
          PRIMARY KEY(id)
        ) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');

        $this->addSql('CREATE TABLE t_user (
          id INT AUTO_INCREMENT NOT NULL, 
          email VARCHAR(180) NOT NULL, 
          password VARCHAR(255) NOT NULL, 
          last_login_at DATETIME DEFAULT NULL, 
          status VARCHAR(20) NOT NULL, 
          confirmation_request_at DATETIME DEFAULT NULL, 
          confirmation_token VARCHAR(255) DEFAULT NULL, 
          confirmation_at DATETIME DEFAULT NULL, 
          created_at DATETIME NOT NULL, 
          updated_at DATETIME NOT NULL, 
          UNIQUE INDEX UNIQ_37E5BF3BE7927C74 (email), 
          PRIMARY KEY(id)
        ) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');

        $this->addSql('CREATE TABLE t_workspace (
          id INT AUTO_INCREMENT NOT NULL, 
          name VARCHAR(150) NOT NULL, 
          presentation LONGTEXT NOT NULL, 
          slugged_name VARCHAR(255) NOT NULL, 
          created_at DATETIME NOT NULL, 
          updated_at DATETIME NOT NULL, 
          UNIQUE INDEX UNIQ_6FE59EF5E237E06 (name), 
          PRIMARY KEY(id)
        ) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');

        $this->addSql('CREATE TABLE t_workspace_user (
          workspace_id INT NOT NULL, 
          user_id INT NOT NULL, 
          INDEX IDX_25374F2882D40A1F (workspace_id), 
          INDEX IDX_25374F28A76ED395 (user_id), 
          PRIMARY KEY(workspace_id, user_id)
        ) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');

        // Foreign keys
        $this->addSql('ALTER TABLE t_event ADD CONSTRAINT FK_851F6CDE82D40A1F FOREIGN KEY (workspace_id) REFERENCES t_workspace (id)');
        $this->addSql('ALTER TABLE t_registration ADD CONSTRAINT FK_15FFC61571F7E88B FOREIGN KEY (event_id) REFERENCES t_event (id)');
        $this->addSql('ALTER TABLE t_workspace_user ADD CONSTRAINT FK_25374F2882D40A1F FOREIGN KEY (workspace_id) REFERENCES t_workspace (id)');
        $this->addSql('ALTER TABLE t_workspace_user ADD CONSTRAINT FK_25374F28A76ED395 FOREIGN KEY (user_id) REFERENCES t_user (id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE t_registration DROP FOREIGN KEY FK_15FFC61571F7E88B');
        $this->addSql('ALTER TABLE t_workspace_user DROP FOREIGN KEY FK_25374F28A76ED395');
        $this->addSql('ALTER TABLE t_event DROP FOREIGN KEY FK_851F6CDE82D40A1F');
        $this->addSql('ALTER TABLE t_workspace_user DROP FOREIGN KEY FK_25374F2882D40A1F');
        $this->addSql('DROP TABLE t_event');
        $this->addSql('DROP TABLE t_registration');
        $this->addSql('DROP TABLE t_user');
        $this->addSql('DROP TABLE t_workspace');
        $this->addSql('DROP TABLE t_workspace_user');
    }
}
