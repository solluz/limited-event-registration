<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20201014063834 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add deregistration token on registration entity';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE t_registration ADD deregistration_token VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE t_registration DROP deregistration_token');
    }
}
