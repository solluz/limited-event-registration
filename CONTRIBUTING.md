CONTRIBUTING
============

Start the application
---------------------

Docker is used for development : [Docker Documentation](https://docs.docker.com/engine/install/debian/)

You also need DockerCompose library : [Docker compose documentation](https://docs.docker.com/engine/install/debian/)

### Build docker containers

Copy `docker-compose.override.yml.dist` to `docker-compose.override.yml`.

Then add `127.0.0.1       event.local` entry to `/etc/hosts`.

Start Docker :

```bash
docker-compose up -d

# Fetch project php dependencies
docker-compose exec app composer install
```

### Initialize Database

```bash
# Check database connection
docker-compose exec app bin/console d:s:v

# Create database
docker-compose exec app bin/console d:s:c

```

### Access web application

https://event.local:8983


Coding standards
----------------

We use the config file **.php_cs.dist** with the version **v2.2** of **friendsofphp/php-cs-fixer**.

```bash
# Display proposed fixes without changing files
php-cs-fixer fix -v --dry-run ./src
#     via docker container
docker-compose exec app php-cs-fixer fix -v --dry-run ./src

# Apply the proposed fixes
php-cs-fixer fix -v ./src
#     via docker container
docker-compose exec app php-cs-fixer fix -v ./src
```


About database schema changes and Doctrine migrations
-----------------------------------------------------

Every change of database schema induces the creation of a Doctrine migration :

```bash
# Generating migration script automatically
docker-compose exec app bin/console doctrine:migrations:diff
```

You need to adapt and format the auto generated migration class.
