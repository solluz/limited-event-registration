README
======

Lievr is an open source application to manage registrations for events with limited places.

In this period of limitations because of COVID19, it allows to manage easily these registrations 
(e.g. for a meeting, a conference, a workshop, a church service).

![Workspace homepage](docs/img/workspace-homepage.png)


Table of content
----------------
- [Requirements](#requirements)
- [Contributing - Development](CONTRIBUTING.md)

Requirements
------------
- Apache2
    - with mod_php
    - with SSL module enabled (and trusted certificates)
- PHP 7.3+
- PHP extensions :
    - ext-ctype
    - ext-dom
    - ext-iconv
    - ext-libxml
- Composer
- MySQL 5.6 server
- Postfix (or other SMTP service)

Installation (Debian)
---------------------

### Deploy

https://deployer.org/

#### Initialize Deployer

You need a php 7.0 version installed on your local machine.

```bash
curl -LO https://deployer.org/deployer.phar
mv deployer.phar ~/App/deployer
chmod +x ~/App/deployer

php7.0 ~/App/deployer --version
```

#### Define host

Create `hosts.yml` to define your deployment host.

```yaml
mydomain.net:
    hostname: mydomain.net
    port: 22
    user: myuser
    stage: prod
    deploy_path: '/var/www/html/myapplication'

```

#### Execute deployment

Use the `deploy.php` script.

```bash
# To deploy on preprod
php7.0 ~/App/deployer deploy staging --tag=v0.2.0

# To deploy on prod
php7.0 ~/App/deployer deploy prod --tag=v1.0.0
```

### Configure .env files

Configure application parameters in .env.local :

```bash
APP_ENV=prod
DATABASE_URL=mysql://db_user:db_password@127.0.0.1:3306/db_name

MAILER_DSN=smtp://localhost
MAILER_SENDER_ADDRESS=no_reply@mydomain.net
```
