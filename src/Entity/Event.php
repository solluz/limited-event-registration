<?php

namespace App\Entity;

use App\Repository\EventRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Table(name="t_event",
 *     uniqueConstraints={@ORM\UniqueConstraint(name="workspace_event_name", columns={"workspace_id", "name"})})
 * @ORM\Entity(repositoryClass=EventRepository::class)
 */
class Event
{
    use TimestampableEntity;

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @var Workspace
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Workspace", inversedBy="events")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="workspace_id", referencedColumnName="id")
     * })
     */
    protected $workspace;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=150)
     */
    protected $name = '';

    /**
     * @var string
     *
     * @ORM\Column(type="text")
     */
    protected $description = '';

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    protected $registrationCapacity = 10;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    protected $registrationEndAt;

    /**
     * @var Registration[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Registration", orphanRemoval=true, mappedBy="event")
     */
    protected $registrations;

    /**
     * Constructor.
     *
     * @param Workspace $workspace
     */
    public function __construct(Workspace $workspace)
    {
        $this->workspace = $workspace;
        $this->workspace->addEvent($this);
        $this->registrationEndAt = date_create('+2 weeks');
        $this->registrations = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Workspace
     */
    public function getWorkspace(): Workspace
    {
        return $this->workspace;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return static
     */
    public function setName(string $name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     *
     * @return static
     */
    public function setDescription(string $description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return int
     */
    public function getRegistrationCapacity(): int
    {
        return $this->registrationCapacity;
    }

    /**
     * @param int $registrationCapacity
     *
     * @return static
     */
    public function setRegistrationCapacity(int $registrationCapacity)
    {
        $this->registrationCapacity = $registrationCapacity;

        return $this;
    }

    /**
     * @return int
     */
    public function getRegistrationAvailableCount()
    {
        return max(0, $this->registrationCapacity - $this->registrations->count());
    }

    /**
     * @return \DateTime
     */
    public function getRegistrationEndAt(): \DateTime
    {
        return $this->registrationEndAt;
    }

    /**
     * @param \DateTime $registrationEndAt
     *
     * @return static
     */
    public function setRegistrationEndAt(\DateTime $registrationEndAt)
    {
        $this->registrationEndAt = $registrationEndAt;

        return $this;
    }

    /**
     * Is the registration closed.
     *
     * @return bool
     */
    public function isRegistrationClosed()
    {
        return $this->registrationEndAt < date_create();
    }

    /**
     * Is the registration full.
     *
     * @return bool
     */
    public function isRegistrationFull()
    {
        return $this->registrationCapacity <= $this->registrations->count();
    }

    /**
     * @return Registration[]|ArrayCollection
     */
    public function getRegistrations()
    {
        return $this->registrations;
    }

    /**
     * @param Registration $registration
     *
     * @return static
     */
    public function addRegistrations(Registration $registration)
    {
        $this->registrations[] = $registration;

        return $this;
    }
}
