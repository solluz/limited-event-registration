<?php

namespace App\Entity;

use App\Repository\RegistrationRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Table(name="t_registration",
 *     uniqueConstraints={@ORM\UniqueConstraint(name="event_registration_name", columns={"event_id", "first_name", "last_name"})})
 * @ORM\Entity(repositoryClass=RegistrationRepository::class)
 */
class Registration
{
    use TimestampableEntity;

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @var Event
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Event", inversedBy="registrations")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="event_id", referencedColumnName="id")
     * })
     */
    protected $event;

    /**
     * @var string
     *
     * @ORM\Column(name="first_name", type="string", length=150)
     */
    protected $firstName = '';

    /**
     * @var string
     *
     * @ORM\Column(name="last_name", type="string", length=150)
     */
    protected $lastName = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     */
    protected $email;

    /**
     * @var string|null
     *
     * @ORM\Column(name="phone_number", type="string", length=50, nullable=true)
     */
    protected $phoneNumber;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="registered_at", type="datetime")
     */
    protected $registeredAt;

    /**
     * If an admin has manually registered this.
     *
     * @var string|null
     *
     * @ORM\Column(name="registered_by", type="string", length=150, nullable=true)
     */
    protected $registeredBy;

    /**
     * @var string|null
     * @ORM\Column(name="deregistration_token", type="string", length=255, nullable=true)
     */
    protected $deregistrationToken;

    /**
     * Constructor.
     *
     * @param Event $event
     */
    public function __construct(Event $event)
    {
        $this->event = $event;
        $this->event->addRegistrations($this);
        $this->registeredAt = date_create();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getFirstName().' '.$this->getLastName();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Event
     */
    public function getEvent(): Event
    {
        return $this->event;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     *
     * @return static
     */
    public function setFirstName(string $firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     *
     * @return static
     */
    public function setLastName(string $lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * @return string
     */
    public function getFullName()
    {
        return sprintf('%s %s', $this->firstName, $this->lastName);
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string|null $email
     *
     * @return static
     */
    public function setEmail(?string $email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }

    /**
     * @param string|null $phoneNumber
     *
     * @return static
     */
    public function setPhoneNumber(?string $phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getRegisteredAt(): \DateTime
    {
        return $this->registeredAt;
    }

    /**
     * @param \DateTime $registeredAt
     *
     * @return static
     */
    public function setRegisteredAt(\DateTime $registeredAt)
    {
        $this->registeredAt = $registeredAt;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getRegisteredBy(): ?string
    {
        return $this->registeredBy;
    }

    /**
     * @param string|null $registeredBy
     *
     * @return static
     */
    public function setRegisteredBy(?string $registeredBy)
    {
        $this->registeredBy = $registeredBy;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDeregistrationToken(): ?string
    {
        return $this->deregistrationToken;
    }

    /**
     * @param string|null $deregistrationToken
     *
     * @return static
     */
    public function setDeregistrationToken(?string $deregistrationToken)
    {
        $this->deregistrationToken = $deregistrationToken;

        return $this;
    }
}
