<?php

namespace App\Entity;

use App\Repository\WorkspaceRepository;
use Ausi\SlugGenerator\SlugGenerator;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Table(name="t_workspace")
 * @ORM\Entity(repositoryClass=WorkspaceRepository::class)
 */
class Workspace
{
    use TimestampableEntity;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=150, unique=true)
     */
    protected $name = '';

    /**
     * @var string
     * @ORM\Column(type="text")
     */
    protected $presentation = '';

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=150, unique=true)
     */
    protected $sluggedName = '';

    /**
     * @var User[]|ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\User", cascade={"persist","remove"}, orphanRemoval=true, inversedBy="workspaces")
     * @ORM\JoinTable(name="t_workspace_user",
     *   joinColumns={
     *     @ORM\JoinColumn(name="workspace_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     *   }
     * )
     */
    protected $users;

    /**
     * @var Event[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Event", orphanRemoval=true, mappedBy="workspace")
     */
    protected $events;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->events = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return Workspace
     */
    public function setName(string $name): self
    {
        $this->name = $name;
        $generator = new SlugGenerator();
        $this->sluggedName = $generator->generate($name);

        return $this;
    }

    /**
     * @return string
     */
    public function getPresentation(): string
    {
        return $this->presentation;
    }

    /**
     * @param string $presentation
     *
     * @return static
     */
    public function setPresentation(string $presentation)
    {
        $this->presentation = $presentation;

        return $this;
    }

    /**
     * @return string
     */
    public function getSluggedName(): string
    {
        return $this->sluggedName;
    }

    /**
     * @return User[]|ArrayCollection
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * @param User $user
     *
     * @return static
     */
    public function addUser(User $user)
    {
        $this->users[] = $user;

        return $this;
    }

    /**
     * @return Event[]|ArrayCollection
     */
    public function getOpenEvents()
    {
        return $this->events->filter(function (Event $event) {return !$event->isRegistrationClosed(); });
    }

    /**
     * @return Event[]|ArrayCollection
     */
    public function getEvents()
    {
        return $this->events;
    }

    /**
     * @param Event $event
     *
     * @return static
     */
    public function addEvent(Event $event)
    {
        $this->events[] = $event;

        return $this;
    }
}
