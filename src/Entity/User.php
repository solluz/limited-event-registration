<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Table(name="t_user")
 * @ORM\Entity(repositoryClass=UserRepository::class)
 */
class User implements UserInterface
{
    use TimestampableEntity;

    /**
     * Available statuses, up to 20 characters.
     */
    const STATUS_WAITING = 'waiting';
    const STATUS_ENABLED = 'enabled';
    const STATUS_DISABLED = 'disabled';

    const STATUSES = [
        self::STATUS_WAITING,
        self::STATUS_ENABLED,
        self::STATUS_DISABLED,
    ];

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $email;

    /**
     * @var string The hashed password
     *
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * Plain password, only used when changing/creating password.
     *
     * @var string
     */
    protected $plainPassword;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="last_login_at", type="datetime", nullable=true)
     */
    protected $lastLoginAt;

    /**
     * @var string
     * @ORM\Column(name="status", type="string", length=20)
     */
    protected $status = self::STATUS_WAITING;

    /**
     * @var \DateTime|null
     * @ORM\Column(name="confirmation_request_at", type="datetime", nullable=true)
     */
    protected $confirmationRequestAt;

    /**
     * @var string|null
     * @ORM\Column(name="confirmation_token", type="string", length=255, nullable=true)
     */
    protected $confirmationToken;

    /**
     * @var \DateTime|null
     * @ORM\Column(name="confirmation_at", type="datetime", nullable=true)
     */
    private $confirmationAt;

    /**
     * @var Workspace[]|ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Workspace", mappedBy="users", cascade={"persist","remove"}),
     */
    protected $workspaces;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->workspaces = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string $email
     *
     * @return User
     */
    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        return ['ROLE_USER'];
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    /**
     * @param string $password
     *
     * @return User
     */
    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @return string
     */
    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    /**
     * @param string|null $plainPassword
     *
     * @return static
     */
    public function setPlainPassword(string $plainPassword = null)
    {
        $this->plainPassword = $plainPassword;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * {@inheritdoc}
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        $this->plainPassword = null;
    }

    /**
     * @return \DateTime|null
     */
    public function getLastLoginAt(): ?\DateTime
    {
        return $this->lastLoginAt;
    }

    /**
     * @param \DateTime|null $lastLoginAt
     *
     * @return static
     */
    public function setLastLoginAt(?\DateTime $lastLoginAt)
    {
        $this->lastLoginAt = $lastLoginAt;

        return $this;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     *
     * @return static
     */
    public function setStatus(string $status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Checks whether the user is enabled.
     *
     * @return bool
     */
    public function isEnabled()
    {
        return self::STATUS_ENABLED === $this->getStatus();
    }

    /**
     * @return \DateTime
     */
    public function getConfirmationRequestAt(): ?\DateTime
    {
        return $this->confirmationRequestAt;
    }

    /**
     * @param \DateTime $confirmationRequestAt
     *
     * @return static
     */
    public function setConfirmationRequestAt(?\DateTime $confirmationRequestAt)
    {
        $this->confirmationRequestAt = $confirmationRequestAt;

        return $this;
    }

    /**
     * @return string
     */
    public function getConfirmationToken(): ?string
    {
        return $this->confirmationToken;
    }

    /**
     * @param ?string $confirmationToken
     *
     * @return static
     */
    public function setConfirmationToken(?string $confirmationToken)
    {
        $this->confirmationToken = $confirmationToken;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getConfirmationAt(): ?\DateTime
    {
        return $this->confirmationAt;
    }

    /**
     * @param \DateTime $confirmationAt
     *
     * @return static
     */
    public function setConfirmationAt(?\DateTime $confirmationAt)
    {
        $this->confirmationAt = $confirmationAt;

        return $this;
    }

    /**
     * @return Workspace[]|ArrayCollection
     */
    public function getWorkspaces()
    {
        return $this->workspaces;
    }

    /**
     * @param Workspace $workspace
     *
     * @return static
     */
    public function addWorkspace(Workspace $workspace)
    {
        $this->workspaces[] = $workspace;
        $workspace->addUser($this);

        return $this;
    }
}
