<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class RegistrationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('workspaces_name', TextType::class, [
                'label' => 'workspace.label.name',
                'help' => 'workspace.help.name',
                'property_path' => 'workspaces[0].name',
                'empty_data' => '',
            ])
            ->add('email', EmailType::class)
            ->add('plainPassword', RepeatedType::class, [
                'type' => PasswordType::class,
                'mapped' => false,
                'invalid_message' => 'user.plain_password.mismatch',
                'first_name' => 'password',
                'second_name' => 'confirmation',
                'first_options' => [
                    'attr' => [
                        'placeholder' => 'user.label.plain_password',
                        'autocomplete' => 'new-password',
                    ],
                    'label' => 'user.label.plain_password',
                    'constraints' => [
                        new NotBlank(),
                        new Length([
                            'min' => 6,
                            'max' => 1000,
                        ]),
                    ],
                ],
                'second_options' => [
                    'attr' => [
                        'placeholder' => 'user.label.plain_password_confirmation',
                        'autocomplete' => 'new-password',
                    ],
                    'label' => 'user.label.plain_password_confirmation',
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'translation_domain' => 'forms',
        ]);
    }
}
