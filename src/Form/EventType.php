<?php

namespace App\Form;

use App\Entity\Event;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EventType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, ['label' => 'event.label.name', 'empty_data' => ''])
            ->add('description', TextareaType::class, [
                'label' => 'event.label.description',
                'required' => false,
                'empty_data' => '',
                'attr' => ['rows' => 5],
            ])
            ->add('registrationCapacity', IntegerType::class, [
                'label' => 'event.label.registration_capacity',
                'help' => 'event.help.registration_capacity',
            ])
            ->add('registrationEndAt', DateTimeType::class, [
                'label' => 'event.label.registration_end_at',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Event::class,
            'translation_domain' => 'forms',
        ]);
    }
}
