<?php

namespace App\Form;

use App\Entity\Registration;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Security;

class RegistrationType extends AbstractType
{
    protected $security;

    /**
     * Constructor.
     *
     * @param Security $security
     */
    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var Registration $registration */
        $registration = $options['data'];

        $builder
            ->add('firstName', TextType::class, ['label' => 'registration.label.first_name', 'empty_data' => ''])
            ->add('lastName', TextType::class, ['label' => 'registration.label.last_name', 'empty_data' => ''])
            ->add('email', EmailType::class, ['label' => 'registration.label.email', 'help' => 'registration.help.email', 'required' => false])
            ->add('phoneNumber', TelType::class, ['label' => 'registration.label.phone_number'])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Registration::class,
            'translation_domain' => 'forms',
        ]);
    }
}
