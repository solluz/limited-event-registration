<?php

namespace App\Form;

use App\Entity\Workspace;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class WorkspaceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, ['label' => 'workspace.label.name', 'disabled' => true])
            ->add('sluggedName', TextType::class, ['label' => 'workspace.label.slugged_name', 'disabled' => true])
            ->add('presentation', TextareaType::class, [
                'label' => 'workspace.label.presentation',
                'help' => 'workspace.help.presentation',
                'required' => false,
                'empty_data' => '',
                'attr' => ['rows' => 5],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Workspace::class,
            'translation_domain' => 'forms',
        ]);
    }
}
