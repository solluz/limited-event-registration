<?php

namespace App\Listener;

use App\Entity\Registration;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Environment;

class SendEventRegistrationConfirmationEmailListener
{
    protected $tokenGenerator;
    protected $mailer;
    protected $translator;
    protected $twig;
    protected $session;

    /**
     * Constructor.
     *
     * @param TokenGeneratorInterface $tokenGenerator
     * @param MailerInterface         $mailer
     * @param TranslatorInterface     $translator
     * @param Environment             $twig
     * @param SessionInterface        $session
     */
    public function __construct(TokenGeneratorInterface $tokenGenerator, MailerInterface $mailer, TranslatorInterface $translator,
                                Environment $twig, SessionInterface $session)
    {
        $this->tokenGenerator = $tokenGenerator;
        $this->mailer = $mailer;
        $this->translator = $translator;
        $this->twig = $twig;
        $this->session = $session;
    }

    /**
     * @param Registration       $registration
     * @param LifecycleEventArgs $event
     */
    public function postPersist(Registration $registration, LifecycleEventArgs $event)
    {
        if (empty($registration->getEmail())) {
            return;
        }

        try {
            $context = ['registration' => $registration];
            $template = $this->twig->load('_mailer/event_registration_confirmation_email.html.twig');
            $subject = trim($template->renderBlock('title', $this->twig->mergeGlobals($context)));

            $this->mailer->send((new TemplatedEmail())
                ->to($registration->getEmail())
                ->subject($subject)
                ->htmlTemplate('_mailer/event_registration_confirmation_email.html.twig')
                ->context($context)
            );

            if ($this->session instanceof Session) {
                $this->session->getFlashBag()->add('success', 'flash.success.registration.confirmation_mail_sent');
            }
        } catch (\Throwable $exception) {
            // do nothing : no confirmation email
        }
    }
}
