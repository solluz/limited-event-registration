<?php

namespace App\Listener;

use App\Entity\Registration;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;

class GenerateDeregistrationTokenListener
{
    protected $tokenGenerator;

    /**
     * Constructor.
     *
     * @param TokenGeneratorInterface $tokenGenerator
     */
    public function __construct(TokenGeneratorInterface $tokenGenerator)
    {
        $this->tokenGenerator = $tokenGenerator;
    }

    /**
     * @param Registration       $registration
     * @param LifecycleEventArgs $event
     */
    public function prePersist(Registration $registration, LifecycleEventArgs $event)
    {
        $registration->setDeregistrationToken($this->tokenGenerator->generateToken());
    }
}
