<?php

namespace App\Command;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserCreateCommand extends Command
{
    protected static $defaultName = 'app:user:create';

    protected $em;

    protected $encoder;

    /**
     * Constructor.
     *
     * @param EntityManagerInterface       $em
     * @param UserPasswordEncoderInterface $encoder
     */
    public function __construct(EntityManagerInterface $em, UserPasswordEncoderInterface $encoder)
    {
        parent::__construct();
        $this->em = $em;
        $this->encoder = $encoder;
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setDescription('Creates a new user.')
            ->addOption('email', null, InputOption::VALUE_REQUIRED, 'The email.')
            ->addOption('password', null, InputOption::VALUE_REQUIRED, 'The plain password.')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $input->isInteractive() ? $io->title('User new') : $io->newLine();

        $user = new User();
        $user->setEmail($input->getOption('email'));
        $user->setPassword($this->encoder->encodePassword($user, $input->getOption('password')));
        $this->em->persist($user);
        $this->em->flush();

        $io->success(sprintf(
            'User created : "%s" : %s',
            $user->getEmail(),
            $user->getId()
        ));

        return 0;
    }
}
