<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

class SendTestEmailCommand extends Command
{
    protected static $defaultName = 'app:send-test-email';

    protected $mailer;

    /**
     * Constructor.
     *
     * @param MailerInterface $mailer
     */
    public function __construct(MailerInterface $mailer)
    {
        parent::__construct();
        $this->mailer = $mailer;
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setDescription('Creates a new user.')
            ->addArgument('recipient', InputArgument::REQUIRED, 'Recipient email address')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $input->isInteractive() ? $io->title('Test sending email') : $io->newLine();

        $helper = $this->getHelper('question');

        $question = new Question('Please enter the email subject', 'Test email');
        $subject = $helper->ask($input, $output, $question);

        $question = new Question('Please enter the raw text content of the email', 'Hello world !');
        $content = $helper->ask($input, $output, $question);
        $to = $input->getArgument('recipient');

        $this->mailer->send((new Email())
            ->to($to)
            ->subject($subject)
            ->text($content)
        );

        $io->success(sprintf('Email sent to "%s"', $to));

        return 0;
    }
}
