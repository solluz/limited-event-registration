<?php

namespace App\Controller;

use App\Entity\Workspace;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class WorkspaceController extends AbstractController
{
    /**
     * @Route("/{sluggedName}", name="workspace")
     *
     * @param Workspace $workspace
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(Workspace $workspace)
    {
        return $this->render('workspace/index.html.twig', [
            'controller_name' => 'WorkspaceController',
            'workspace' => $workspace,
        ]);
    }
}
