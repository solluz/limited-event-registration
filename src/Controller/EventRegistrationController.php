<?php

namespace App\Controller;

use App\Entity\Event;
use App\Entity\Registration;
use App\Entity\Workspace;
use App\Form\RegistrationType;
use App\Repository\RegistrationRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class EventRegistrationController extends AbstractController
{
    protected $em;
    protected $registrationRepository;

    /**
     * Constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em, RegistrationRepository $registrationRepository)
    {
        $this->em = $em;
        $this->registrationRepository = $registrationRepository;
    }

    /**
     * @Route("/{sluggedName}/{event}/register", name="event_registration_register", methods={"GET", "POST"})
     *
     * @param Request   $request
     * @param Workspace $workspace
     * @param Event     $event
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function register(Request $request, Workspace $workspace, Event $event)
    {
        if ($event->getWorkspace() !== $workspace) {
            $this->createNotFoundException();
        }
        if ($event->isRegistrationClosed()) {
            $this->addFlash('warning', 'flash.warning.event.closed');

            return $this->redirectToRoute('workspace', ['sluggedName' => $event->getWorkspace()->getSluggedName()]);
        }
        if ($event->isRegistrationFull()) {
            $this->addFlash('warning', 'flash.warning.event.full');

            return $this->redirectToRoute('workspace', ['sluggedName' => $event->getWorkspace()->getSluggedName()]);
        }

        $registration = new Registration($event);
        $form = $this->createForm(RegistrationType::class, $registration);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($registration);
            $this->em->flush();
            $this->addFlash('success', 'flash.success.event_registration.registered');

            return $this->redirectToRoute('workspace', ['sluggedName' => $event->getWorkspace()->getSluggedName()]);
        }

        return $this->render('event_registration/register.html.twig', [
            'workspace' => $workspace,
            'event' => $event,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{sluggedName}/{event}/deregister/{token}", name="event_registration_deregister", methods={"GET", "POST"})
     *
     * @param Request   $request
     * @param Workspace $workspace
     * @param Event     $event
     * @param string    $token
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deregister(Request $request, Workspace $workspace, Event $event, string $token)
    {
        if ($event->getWorkspace() !== $workspace) {
            throw $this->createNotFoundException('Invalid event.');
        }
        if ($event->isRegistrationClosed()) {
            $this->addFlash('warning', 'flash.warning.event.closed');

            return $this->redirectToRoute('workspace', ['sluggedName' => $workspace->getSluggedName()]);
        }

        $registration = $this->registrationRepository->findOneBy(['deregistrationToken' => $token]);
        if (!$registration instanceof Registration || $registration->getEvent() !== $event) {
            throw $this->createNotFoundException('Invalid registration.');
        }

        $form = $this->createFormBuilder()->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->remove($registration);
            $this->em->flush();
            $this->addFlash('success', 'flash.success.registration.deregistered');

            return $this->redirectToRoute('workspace', ['sluggedName' => $workspace->getSluggedName()]);
        }

        return $this->render('event_registration/deregister.html.twig', [
            'workspace' => $workspace,
            'registration' => $registration,
            'form' => $form->createView(),
        ]);
    }
}
