<?php

namespace App\Controller\Security;

use App\Entity\User;
use App\Entity\Workspace;
use App\Form\RegistrationFormType;
use App\Repository\UserRepository;
use App\Security\RegistrationHandler;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class RegistrationController extends AbstractController
{
    protected $registrationHandler;

    /**
     * Constructor.
     *
     * @param RegistrationHandler $registrationHandler
     */
    public function __construct(RegistrationHandler $registrationHandler)
    {
        $this->registrationHandler = $registrationHandler;
    }

    /**
     * @Route("/registration/register", methods={"GET", "POST"}, name="security_registration_register")
     *
     * @param Request                      $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     *
     * @return Response
     *
     * @throws \Symfony\Component\Mailer\Exception\TransportExceptionInterface
     */
    public function registerAction(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        if ($this->getUser() instanceof User) {
            return $this->redirectToRoute('homepage');
        }

        $user = new User();
        $workspace = new Workspace();
        $user->addWorkspace($workspace);
        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // encode the plain password
            $user->setPassword(
                $passwordEncoder->encodePassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );
            $this->registrationHandler->initialize($user);
            $this->addFlash('success', 'flash.success.security.registration.confirmation_mail_sent');

            return $this->redirectToRoute('security_login');
        }

        return $this->render('registration/register.html.twig', [
            'registrationForm' => $form->createView(),
        ]);
    }

    /**
     * @Route("/registration/confirm/{token}", methods={"GET", "POST"}, name="security_registration_confirm")
     *
     * @param mixed          $token
     * @param UserRepository $userRepository
     *
     * @return Response
     *
     * @throws \Symfony\Component\Mailer\Exception\TransportExceptionInterface
     */
    public function confirmAction($token, UserRepository $userRepository)
    {
        $user = $userRepository->findOneBy(['confirmationToken' => $token]);
        if (!$user instanceof User) {
            $this->addFlash('warning', 'flash.warning.security.registration.invalid_token');

            return $this->redirectToRoute('security_login');
        }
        if ($this->registrationHandler->isConfirmationRequestExpired($user)) {
            $this->addFlash('warning', 'flash.warning.security.registration.expired_token');
            $this->registrationHandler->initialize($user);
            $this->addFlash('success', 'flash.success.security.registration.confirmation_mail_sent');

            return $this->redirectToRoute('security_login');
        }

        $this->registrationHandler->finalize($user);
        $this->addFlash('success', 'flash.success.security.registration.confirmed');

        return $this->redirectToRoute('security_login');
    }
}
