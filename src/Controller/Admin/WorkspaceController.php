<?php

namespace App\Controller\Admin;

use App\Entity\Workspace;
use App\Form\WorkspaceType;
use App\Security\Voter\WorkspaceVoter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin")
 */
class WorkspaceController extends AbstractController
{
    /**
     * @Route("/workspace/{sluggedName}/edit", name="admin_workspace_edit")
     * @IsGranted(WorkspaceVoter::EDIT, subject="workspace")
     *
     * @param Request   $request
     * @param Workspace $workspace
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(Request $request, Workspace $workspace)
    {
        $form = $this->createForm(WorkspaceType::class, $workspace);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($workspace);
            $em->flush();
            $this->addFlash('success', 'flash.success.workspace.updated');

            return $this->redirectToRoute('workspace', ['sluggedName' => $workspace->getSluggedName()]);
        }

        return $this->render('admin/workspace/edit.html.twig', [
            'form' => $form->createView(),
            'workspace' => $workspace,
        ]);
    }
}
