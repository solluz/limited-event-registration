<?php

namespace App\Controller\Admin;

use App\Entity\Event;
use App\Entity\Registration;
use App\Entity\Workspace;
use App\Form\RegistrationType;
use App\Repository\RegistrationRepository;
use App\Security\Voter\EventVoter;
use App\Security\Voter\RegistrationVoter;
use Doctrine\ORM\EntityManagerInterface;
use Kilik\TableBundle\Components\Column;
use Kilik\TableBundle\Components\Filter;
use Kilik\TableBundle\Components\Table;
use Kilik\TableBundle\Services\TableService;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/admin/registrations")
 */
class RegistrationController extends AbstractController
{
    protected $em;

    protected $kilik;

    protected $registrationRepository;

    /**
     * Constructor.
     *
     * @param EntityManagerInterface $em
     * @param TableService           $kilik
     * @param RegistrationRepository $registrationRepository
     */
    public function __construct(EntityManagerInterface $em, TableService $kilik, RegistrationRepository $registrationRepository)
    {
        $this->em = $em;
        $this->kilik = $kilik;
        $this->registrationRepository = $registrationRepository;
    }

    /**
     * @Route("/{event}", name="admin_registration_list", methods={"GET"})
     * @IsGranted(EventVoter::LIST_REGISTRATION, subject="event")
     *
     * @param Event $event
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function list(Event $event)
    {
        return $this->render('admin/registration/list.html.twig', [
            'table' => $this->kilik->createFormView($this->getTable($event)),
            'workspace' => $event->getWorkspace(),
            'event' => $event,
        ]);
    }

    /**
     * @Route("/{event}/_list", name="admin_registration_list_ajax")
     * @IsGranted(EventVoter::LIST_REGISTRATION, subject="event")
     *
     * @param Request $request
     * @param Event   $event
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Throwable
     */
    public function _list(Request $request, Event $event)
    {
        return $this->kilik->handleRequest($this->getTable($event), $request);
    }

    /**
     * @Route("/{event}/export", name="admin_registration_export_excel", methods={"GET"})
     * @IsGranted(EventVoter::LIST_REGISTRATION, subject="event")
     *
     * @param Event               $event
     * @param TranslatorInterface $translator
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function exportExcel(Event $event, TranslatorInterface $translator)
    {
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $sheet->setCellValueByColumnAndRow(1, 1, $translator->trans('registration.label.last_name', [], 'forms'));
        $sheet->setCellValueByColumnAndRow(2, 1, $translator->trans('registration.label.first_name', [], 'forms'));
        $sheet->setCellValueByColumnAndRow(3, 1, $translator->trans('registration.label.email', [], 'forms'));
        $sheet->setCellValueByColumnAndRow(4, 1, $translator->trans('registration.label.phone_number', [], 'forms'));
        $sheet->setCellValueByColumnAndRow(5, 1, $translator->trans('registration.label.registered_at', [], 'forms'));

        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);

        $row = 2;
        foreach ($event->getRegistrations() as $registration) {
            $sheet->setCellValueByColumnAndRow(1, $row, $registration->getLastName());
            $sheet->setCellValueByColumnAndRow(2, $row, $registration->getFirstName());
            $sheet->setCellValueByColumnAndRow(3, $row, $registration->getEmail());
            $sheet->setCellValueByColumnAndRow(4, $row, $registration->getPhoneNumber());
            $sheet->setCellValueByColumnAndRow(5, $row, $registration->getRegisteredAt())
            ;
            ++$row;
        }

        $writer = new Xlsx($spreadsheet);
        $response = new StreamedResponse(
            function () use ($writer) {
                $writer->save('php://output');
            }
        );
        $response->headers->set('Content-Type', 'application/vnd.ms-excel');
        $response->headers->set('Content-Disposition', 'attachment;filename="Export.xlsx"');
        $response->headers->set('Cache-Control', 'max-age=0');

        return $response;
    }

    /**
     * @Route("/{event}/new", name="admin_registration_create", methods={"GET", "POST"})
     * @IsGranted(EventVoter::CREATE_REGISTRATION, subject="event")
     *
     * @param Request $request
     * @param Event   $event
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(Request $request, Event $event)
    {
        $registration = new Registration($event);
        $registration->setRegisteredBy($this->getUser()->getUsername());
        $form = $this->createForm(RegistrationType::class, $registration);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($registration);
            $this->em->flush();
            $this->addFlash('success', 'flash.success.registration.created');

            return $this->redirectToRoute('admin_registration_edit', ['event' => $event->getId(), 'registration' => $registration->getId()]);
        }

        return $this->render('admin/registration/create.html.twig', [
            'workspace' => $event->getWorkspace(),
            'registration' => $registration,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{event}/delete-all", name="admin_registration_delete_all", methods={"GET", "DELETE"})
     * @IsGranted(EventVoter::EDIT, subject="event")
     *
     * @param Request $request
     * @param Event   $event
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deleteAll(Request $request, Event $event)
    {
        if (!$event->isRegistrationClosed()) {
            $this->addFlash('warning', 'flash.warning.registration.cannot_delete_all_open_event');

            return $this->redirectToRoute('admin_registration_list', ['event' => $event->getId()]);
        }

        $form = $this->createFormBuilder()->setMethod(Request::METHOD_DELETE)->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            foreach ($event->getRegistrations() as $registration) {
                $this->em->remove($registration);
            }
            $this->em->flush();
            $this->addFlash('success', 'flash.success.registration.all_deleted');

            return $this->redirectToRoute('admin_registration_list', ['event' => $event->getId()]);
        }

        return $this->render('admin/registration/delete_all.html.twig', [
            'workspace' => $event->getWorkspace(),
            'event' => $event,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{event}/{registration}", name="admin_registration_edit", methods={"GET", "POST"})
     * @IsGranted(RegistrationVoter::EDIT, subject="registration")
     *
     * @param Request      $request
     * @param Event        $event
     * @param Registration $registration
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(Request $request, Event $event, Registration $registration)
    {
        $form = $this->createForm(RegistrationType::class, $registration);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($registration);
            $this->em->flush();
            $this->addFlash('success', 'flash.success.registration.updated');

            return $this->redirectToRoute('admin_registration_edit', ['event' => $event->getId(), 'registration' => $registration->getId()]);
        }

        return $this->render('admin/registration/edit.html.twig', [
            'workspace' => $event->getWorkspace(),
            'registration' => $registration,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{sluggedName}/{registration}/delete", name="admin_registration_delete", methods={"GET", "DELETE"})
     * @IsGranted(RegistrationVoter::DELETE, subject="registration")
     *
     * @param Request      $request
     * @param Workspace    $workspace
     * @param Registration $registration
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function delete(Request $request, Workspace $workspace, Registration $registration)
    {
        $form = $this->createFormBuilder()->setMethod(Request::METHOD_DELETE)->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->remove($registration);
            $this->em->flush();
            $this->addFlash('success', 'flash.success.registration.deleted');

            return $this->redirectToRoute('admin_registration_list', ['event' => $registration->getEvent()->getId()]);
        }

        return $this->render('admin/registration/delete.html.twig', [
            'workspace' => $workspace,
            'registration' => $registration,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @param Event $event
     *
     * @return Table
     */
    protected function getTable(Event $event)
    {
        $qb = $this->registrationRepository->createQueryBuilder('registration');
        $qb
            ->andWhere($qb->expr()->eq('registration.event', ':event'))
            ->setParameter('event', $event->getId())
        ;

        $table = (new Table())
            ->setId('admin_registration_list')
            ->setPath($this->generateUrl('admin_registration_list_ajax', ['event' => $event->getId()]))
            ->setQueryBuilder($qb, 'registration')
            ->setDefaultIdentifierFieldNames()
            ->setEntityLoaderRepository('App:Registration')
            ->setTemplate('admin/registration/_table.html.twig')
            ->setTemplateParams([
                'workspace' => $event->getWorkspace(),
                'event' => $event,
            ])
            ->addColumn(
                (new Column())->setLabel('registration.label.first_name')->setTranslateDomain('forms')
                    ->setSort(['registration.firstName' => 'asc'])
                    ->setFilter((new Filter())
                        ->setField('registration.firstName')
                        ->setName('registration_firstName')
                    )
            )
            ->addColumn(
                (new Column())->setLabel('registration.label.last_name')->setTranslateDomain('forms')
                    ->setSort(['registration.lastName' => 'asc'])
                    ->setFilter((new Filter())
                        ->setField('registration.lastName')
                        ->setName('registration_lastName')
                    )
            )
            ->addColumn(
                (new Column())->setLabel('registration.label.email')->setTranslateDomain('forms')
                    ->setSort(['registration.email' => 'asc'])
                    ->setFilter((new Filter())
                        ->setField('registration.email')
                        ->setName('registration_email')
                    )
            )
            ->addColumn(
                (new Column())->setLabel('registration.label.phone_number')->setTranslateDomain('forms')
                    ->setSort(['registration.phoneNumber' => 'asc'])
                    ->setFilter((new Filter())
                        ->setField('registration.phoneNumber')
                        ->setName('registration_phoneNumber')
                    )
            )
            ->addColumn(
                (new Column())->setLabel('registration.label.registered_at')->setTranslateDomain('forms')
                    ->setSort(['registration.registeredAt' => 'asc'])
                    ->setDisplayFormat(Column::FORMAT_DATE)
                    ->setDisplayFormatParams('Y-m-d H:i:s')
                    ->setFilter((new Filter())
                        ->setName('registration_registeredAt')
                        ->setField("DATE_FORMAT(registration.registeredAt, 'YYYY-MM-DD HH24:MI:SS')")
                    )
            )
        ;

        return $table;
    }
}
