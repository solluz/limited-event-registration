<?php

namespace App\Controller\Admin;

use App\Entity\Event;
use App\Entity\Workspace;
use App\Form\EventType;
use App\Repository\EventRepository;
use App\Security\Voter\EventVoter;
use App\Security\Voter\WorkspaceVoter;
use Doctrine\ORM\EntityManagerInterface;
use Kilik\TableBundle\Components\Column;
use Kilik\TableBundle\Components\Filter;
use Kilik\TableBundle\Components\Table;
use Kilik\TableBundle\Services\TableService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/events")
 */
class EventController extends AbstractController
{
    protected $em;

    protected $kilik;

    protected $eventRepository;

    /**
     * Constructor.
     *
     * @param EntityManagerInterface $em
     * @param TableService           $kilik
     * @param EventRepository        $eventRepository
     */
    public function __construct(EntityManagerInterface $em, TableService $kilik, EventRepository $eventRepository)
    {
        $this->em = $em;
        $this->kilik = $kilik;
        $this->eventRepository = $eventRepository;
    }

    /**
     * @Route("/{sluggedName}", name="admin_event_list", methods={"GET"})
     * @IsGranted(WorkspaceVoter::LIST_EVENT, subject="workspace")
     *
     * @param Workspace $workspace
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function list(Workspace $workspace)
    {
        return $this->render('admin/event/list.html.twig', [
            'table' => $this->kilik->createFormView($this->getTable($workspace)),
            'workspace' => $workspace,
        ]);
    }

    /**
     * @Route("/{sluggedName}/_list", name="admin_event_list_ajax")
     * @IsGranted(WorkspaceVoter::LIST_EVENT, subject="workspace")
     *
     * @param Request   $request
     * @param Workspace $workspace
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Throwable
     */
    public function _list(Request $request, Workspace $workspace)
    {
        return $this->kilik->handleRequest($this->getTable($workspace), $request);
    }

    /**
     * @Route("/{sluggedName}/new", name="admin_event_create", methods={"GET", "POST"})
     * @IsGranted(WorkspaceVoter::CREATE_EVENT, subject="workspace")
     *
     * @param Request   $request
     * @param Workspace $workspace
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(Request $request, Workspace $workspace)
    {
        $event = new Event($workspace);
        $form = $this->createForm(EventType::class, $event);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($event);
            $this->em->flush();
            $this->addFlash('success', 'flash.success.event.created');

            return $this->redirectToRoute('admin_event_edit', ['sluggedName' => $workspace->getSluggedName(), 'event' => $event->getId()]);
        }

        return $this->render('admin/event/create.html.twig', [
            'workspace' => $workspace,
            'event' => $event,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{sluggedName}/{event}", name="admin_event_edit", methods={"GET", "POST"})
     * @IsGranted(EventVoter::EDIT, subject="event")
     *
     * @param Request   $request
     * @param Workspace $workspace
     * @param Event     $event
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(Request $request, Workspace $workspace, Event $event)
    {
        $form = $this->createForm(EventType::class, $event);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($event);
            $this->em->flush();
            $this->addFlash('success', 'flash.success.event.updated');

            return $this->redirectToRoute('admin_event_edit', ['sluggedName' => $workspace->getSluggedName(), 'event' => $event->getId()]);
        }

        return $this->render('admin/event/edit.html.twig', [
            'workspace' => $workspace,
            'event' => $event,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{sluggedName}/{event}/delete", name="admin_event_delete", methods={"GET", "DELETE"})
     * @IsGranted(EventVoter::DELETE, subject="event")
     *
     * @param Request   $request
     * @param Workspace $workspace
     * @param Event     $event
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function delete(Request $request, Workspace $workspace, Event $event)
    {
        if (!$event->isRegistrationClosed()) {
            $this->addFlash('warning', 'flash.warning.event.cannot_delete_open_event');

            return $this->redirectToRoute('admin_event_edit', ['sluggedName' => $workspace->getSluggedName(), 'event' => $event->getId()]);
        }

        $form = $this->createFormBuilder()->setMethod(Request::METHOD_DELETE)->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->remove($event);
            $this->em->flush();
            $this->addFlash('success', 'flash.success.event.deleted');

            return $this->redirectToRoute('admin_event_list', ['sluggedName' => $workspace->getSluggedName()]);
        }

        return $this->render('admin/event/delete.html.twig', [
            'workspace' => $workspace,
            'event' => $event,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @param Workspace $workspace
     *
     * @return Table
     */
    protected function getTable(Workspace $workspace)
    {
        $qb = $this->eventRepository->createQueryBuilder('event');
        $qb
            ->addSelect('COUNT(registration.id) AS registration_count')
            ->leftJoin('event.registrations', 'registration')
            ->andWhere($qb->expr()->eq('event.workspace', ':workspace'))
            ->addGroupBy('event.id')
            ->setParameter('workspace', $workspace->getId())
        ;

        $table = (new Table())
            ->setId('admin_event_list')
            ->setPath($this->generateUrl('admin_event_list_ajax', ['sluggedName' => $workspace->getSluggedName()]))
            ->setQueryBuilder($qb, 'event')
            ->setEntityLoaderRepository('App:Event')
            ->setTemplate('_table/_table.html.twig')
            ->setTemplateParams([
                'show_route_name' => 'admin_event_edit',
                'show_route_identifier_name' => 'event',
                'workspace' => $workspace,
            ])
            ->addColumn(
                (new Column())->setLabel('event.label.name')->setTranslateDomain('forms')
                    ->setSort(['event.name' => 'asc'])
                    ->setFilter((new Filter())
                        ->setField('event.name')
                        ->setName('event_name')
                    )
            )
            ->addColumn(
                (new Column())->setLabel('event.label.registration_capacity')->setTranslateDomain('forms')
                    ->setSort(['event.registrationCapacity' => 'asc'])
                    ->setFilter((new Filter())
                        ->setField('event.registrationCapacity')
                        ->setName('event_registrationCapacity')
                    )
            )
            ->addColumn(
                (new Column())->setLabel('event.label.registration_count')->setTranslateDomain('forms')
                    ->setSort(['registration_count' => 'asc'])
                    ->setFilter((new Filter())
                        ->setField('registration_count')
                        ->setName('registration_count')
                        ->setHaving(true)
                    )
                    ->setDisplayCallback(function ($value, $row) {
                        return $this->renderView('registration/_registration_count_column.html.twig', [
                            'registration_count' => $value,
                            'event' => $row['object'],
                        ]);
                    })
                    ->setRaw(true)
            )
            ->addColumn(
                (new Column())->setLabel('event.label.registration_end_at')->setTranslateDomain('forms')
                    ->setSort(['event.registrationEndAt' => 'asc'])
                    ->setDisplayFormat(Column::FORMAT_DATE)
                    ->setDisplayFormatParams('Y-m-d H:i:s')
                    ->setFilter((new Filter())
                        ->setName('event_registrationEndAt')
                        ->setField("DATE_FORMAT(event.registrationEndAt, 'YYYY-MM-DD HH24:MI:SS')")
                    )
            )
        ;

        return $table;
    }
}
