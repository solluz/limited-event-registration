<?php

namespace App\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="homepage")
     */
    public function index()
    {
        $user = $this->getUser();
        if (!$user instanceof User) {
            return $this->redirectToRoute('security_login');
        }

        return $this->redirectToRoute('workspace', ['sluggedName' => $user->getWorkspaces()->first()->getSluggedName()]);
    }
}
