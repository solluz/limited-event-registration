<?php

namespace App\Security\Voter;

use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

abstract class AbstractObjectVoter extends Voter
{
    /**
     * Fully qualified class name of the managed object.
     */
    const OBJECT_CLASS_NAME = '';

    /**
     * Supported attributes for this voter.
     *
     * e.g. Vote on attribute `enable_feature' will call method `canEnableFeature($subject, TokenInterface $token)`
     */
    const SUPPORTED_ATTRIBUTES = [];

    /**
     * Supported attributes that does not need subject.
     *
     * e.g. Vote on attribute `createApp\Core\Entity\User' will call method `canCreate(TokenInterface $token)`
     */
    const SUPPORTED_ATTRIBUTES_WITHOUT_SUBJECT = [];

    /**
     * @var AccessDecisionManagerInterface
     */
    protected $decisionManager;

    /**
     * @param AccessDecisionManagerInterface $decisionManager
     */
    public function __construct(AccessDecisionManagerInterface $decisionManager)
    {
        $this->decisionManager = $decisionManager;

        if (self::OBJECT_CLASS_NAME === static::OBJECT_CLASS_NAME) {
            throw new \RuntimeException(sprintf('Expect %s::OBJECT_CLASS_NAME constant to be defined', static::class));
        }
    }

    /**
     * Checks if the role is granted.
     *
     * @param string         $role
     * @param TokenInterface $token
     *
     * @return bool
     */
    final public function isRoleGranted($role, TokenInterface $token)
    {
        return $this->decisionManager->decide($token, [$role]);
    }

    /**
     * {@inheritdoc}
     */
    final protected function supports($attribute, $subject)
    {
        if (\in_array($attribute, static::SUPPORTED_ATTRIBUTES_WITHOUT_SUBJECT)) {
            return null === $subject;
        }

        if (!\in_array($attribute, static::SUPPORTED_ATTRIBUTES)) {
            return false;
        }

        if (!is_a($subject, static::OBJECT_CLASS_NAME)) {
            return false;
        }

        return true;
    }

    /**
     * {@inheritdoc}
     */
    final protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $methodName = $this->buildMethodName($attribute);

        if (!\is_callable([$this, $methodName])) {
            throw new \LogicException(sprintf('Voter method %s::%s() not implemented.', static::class, $attribute));
        }

        if (\in_array($attribute, static::SUPPORTED_ATTRIBUTES_WITHOUT_SUBJECT)) {
            return \call_user_func([$this, $methodName], $token);
        }

        return \call_user_func([$this, $methodName], $subject, $token);
    }

    /**
     * Transform an attribute like `list_object` to `canListObject` method name.
     *
     * @param $attribute
     *
     * @return string
     */
    final protected function buildMethodName($attribute)
    {
        $methodName = $attribute;
        $methodName = str_replace(static::OBJECT_CLASS_NAME, '', $methodName);
        $methodName = str_replace(' ', '', ucwords(str_replace('_', ' ', $methodName)));

        return sprintf('can%s', (string) $methodName);
    }

    /**
     * Gets current user.
     *
     * @param TokenInterface $token
     *
     * @return \App\Entity\User|null
     */
    final protected function getUser(TokenInterface $token)
    {
        $user = $token->getUser();

        if ($user instanceof User) {
            return $user;
        }

        return null;
    }

    /**
     * Gets current user workspaces.
     *
     * @param TokenInterface $token
     *
     * @return \App\Entity\Workspace[]
     */
    final protected function getUserWorkspaces(TokenInterface $token)
    {
        $user = $this->getUser($token);

        if (!$user instanceof User) {
            return [];
        }

        return $user->getWorkspaces()->toArray();
    }
}
