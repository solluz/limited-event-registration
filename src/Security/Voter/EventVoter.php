<?php

namespace App\Security\Voter;

use App\Entity\Event;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

final class EventVoter extends AbstractObjectVoter
{
    const OBJECT_CLASS_NAME = Event::class;

    // Attributes
    const CREATE_REGISTRATION = 'create_registration'.self::OBJECT_CLASS_NAME;
    const DELETE = 'delete'.self::OBJECT_CLASS_NAME;
    const EDIT = 'edit'.self::OBJECT_CLASS_NAME;
    const LIST_REGISTRATION = 'list_registration'.self::OBJECT_CLASS_NAME;

    const SUPPORTED_ATTRIBUTES = [
        self::CREATE_REGISTRATION,
        self::DELETE,
        self::EDIT,
        self::LIST_REGISTRATION,
    ];

    /**
     * @param Event          $subject
     * @param TokenInterface $token
     *
     * @return bool
     */
    protected function canCreateRegistration($subject, TokenInterface $token): bool
    {
        return $this->canEdit($subject, $token);
    }

    /**
     * @param Event          $subject
     * @param TokenInterface $token
     *
     * @return bool
     */
    protected function canEdit($subject, TokenInterface $token): bool
    {
        return in_array($subject->getWorkspace(), $this->getUserWorkspaces($token));
    }

    /**
     * @param Event          $subject
     * @param TokenInterface $token
     *
     * @return bool
     */
    protected function canDelete($subject, TokenInterface $token): bool
    {
        return $this->canEdit($subject, $token);
    }

    /**
     * @param Event          $subject
     * @param TokenInterface $token
     *
     * @return bool
     */
    protected function canListRegistration($subject, TokenInterface $token): bool
    {
        return $this->canEdit($subject, $token);
    }
}
