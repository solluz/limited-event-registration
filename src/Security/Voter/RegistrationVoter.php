<?php

namespace App\Security\Voter;

use App\Entity\Registration;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

final class RegistrationVoter extends AbstractObjectVoter
{
    const OBJECT_CLASS_NAME = Registration::class;

    // Attributes
    const DELETE = 'delete'.self::OBJECT_CLASS_NAME;
    const EDIT = 'edit'.self::OBJECT_CLASS_NAME;

    const SUPPORTED_ATTRIBUTES = [
        self::DELETE,
        self::EDIT,
    ];

    /**
     * @param Registration   $subject
     * @param TokenInterface $token
     *
     * @return bool
     */
    protected function canEdit($subject, TokenInterface $token): bool
    {
        return in_array($subject->getEvent()->getWorkspace(), $this->getUserWorkspaces($token));
    }

    /**
     * @param Registration   $subject
     * @param TokenInterface $token
     *
     * @return bool
     */
    protected function canDelete($subject, TokenInterface $token): bool
    {
        return $this->canEdit($subject, $token);
    }
}
