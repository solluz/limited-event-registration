<?php

namespace App\Security\Voter;

use App\Entity\Workspace;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

final class WorkspaceVoter extends AbstractObjectVoter
{
    const OBJECT_CLASS_NAME = Workspace::class;

    // Attributes
    const CREATE_EVENT = 'create_event'.self::OBJECT_CLASS_NAME;
    const EDIT = 'edit'.self::OBJECT_CLASS_NAME;
    const LIST_EVENT = 'list_event'.self::OBJECT_CLASS_NAME;

    const SUPPORTED_ATTRIBUTES = [
        self::CREATE_EVENT,
        self::EDIT,
        self::LIST_EVENT,
    ];

    /**
     * @param Workspace      $subject
     * @param TokenInterface $token
     *
     * @return bool
     */
    protected function canCreateEvent($subject, TokenInterface $token): bool
    {
        return $this->canEdit($subject, $token);
    }

    /**
     * @param Workspace      $subject
     * @param TokenInterface $token
     *
     * @return bool
     */
    protected function canEdit($subject, TokenInterface $token): bool
    {
        return in_array($subject, $this->getUserWorkspaces($token));
    }

    /**
     * @param Workspace      $subject
     * @param TokenInterface $token
     *
     * @return bool
     */
    protected function canListEvent($subject, TokenInterface $token): bool
    {
        return $this->canEdit($subject, $token);
    }
}
