<?php

namespace App\Security;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class RegistrationHandler
{
    /**
     * Time to live of confirmation token in seconds.
     */
    const CONFIRMATION_TOKEN_TTL = 172800;

    protected $em;
    protected $tokenGenerator;
    protected $mailer;
    protected $translator;

    /**
     * PasswordResettingHandler constructor.
     *
     * @param EntityManagerInterface  $em
     * @param TokenGeneratorInterface $tokenGenerator
     * @param MailerInterface         $mailer
     * @param TranslatorInterface     $translator
     */
    public function __construct(EntityManagerInterface $em, TokenGeneratorInterface $tokenGenerator, MailerInterface $mailer,
                                TranslatorInterface $translator)
    {
        $this->em = $em;
        $this->tokenGenerator = $tokenGenerator;
        $this->mailer = $mailer;
        $this->translator = $translator;
    }

    /**
     * Initializes new user registration.
     *
     * @param User $user
     *
     * @throws \Symfony\Component\Mailer\Exception\TransportExceptionInterface
     */
    public function initialize(User $user)
    {
        if (User::STATUS_DISABLED === $user->getStatus()) {
            throw new \InvalidArgumentException('Disabled user');
        }

        $user
            ->setConfirmationToken($this->tokenGenerator->generateToken())
            ->setConfirmationRequestAt(date_create())
            ->setStatus(User::STATUS_WAITING)
        ;

        $this->em->persist($user);
        $this->em->flush();

        $this->mailer->send((new TemplatedEmail())
            ->to($user->getEmail())
            ->subject($this->translator->trans('security.registration.confirm_email.title', [], 'mailer'))
            ->htmlTemplate('_mailer/security_registration_confirm_email.html.twig')
            ->context(['user' => $user])
        );
    }

    /**
     * @param User $user
     */
    public function finalize(User $user)
    {
        $user->setConfirmationToken(null);
        $user->setConfirmationAt(date_create());
        if (User::STATUS_WAITING == $user->getStatus()) {
            $user->setStatus(User::STATUS_ENABLED);
        }
        $this->em->persist($user);
        $this->em->flush();
    }

    /**
     * @param User $user
     *
     * @return bool
     */
    public function isConfirmationRequestExpired(User $user)
    {
        return !$user->getConfirmationRequestAt() instanceof \DateTime ||
            $user->getConfirmationRequestAt()->getTimestamp() + static::CONFIRMATION_TOKEN_TTL < time();
    }
}
