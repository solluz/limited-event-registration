<?php
namespace Deployer;

require 'recipe/symfony4.php';

// Project name
set('application', 'lievr');

// Project repository
set('repository', 'https://benito103e@bitbucket.org/solluz/limited-event-registration.git');
set('keep_releases', 3);
set('git_tty', true);
set('writable_mode', 'chmod');

// Shared files
set('shared_dirs', ['var/log', 'var/sessions']);
set('shared_files', ['.env.local']);

// Disable anonymous stats
set('allow_anonymous_stats', false);

// Fix backward compatibility of bin/console for symfony4 recipe
set('bin/console', function () {
    return parse('{{release_path}}/bin/console');
});
set('console_options', function () {
    return '--no-interaction';
});

// Task : 'dump:env'
set('symfony_env', 'prod');
task('dump:env', function () {
    run('cd {{release_path}} && {{bin/composer}} dump-env {{symfony_env}}');
});

// Add tasks to default symfony installation
after('deploy:vendors', 'dump:env');
after('dump:env', 'database:migrate');

inventory('hosts.yml');
